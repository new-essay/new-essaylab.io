---
layout: mypost
title: Minecraft Optimization
categories: [Game]
---

# OS

- 版本 Windows 10 专业版
- 版本号 20H2
- 安装日期 ‎2020/‎5/‎30
- 操作系统内部版本 19042.928
- 体验 Windows Feature Experience Pack 120.2212.551.0

# JRE

- openjdk 11.0.10 2021-01-19
- OpenJDK Runtime Environment Microsoft-18724 (build 11.0.10+9)
- OpenJDK 64-Bit Server VM Microsoft-18724 (build 11.0.10+9, mixed mode)

# Minecraft

Minecraft 1.16.5

# Mod Loader

Fabric Loader 0.11.3

# Optimization Mods

- EntityCulling-Fabric 1.2.1
- FerriteCore 2.0.4
- Hydrogen 0.2
- Lithium 0.6.4
- Smooth Boot 1.16.5-1.6.0
- Sodium 0.1.0
- Starlight fabric-1.0.0-RC1-59d1f21 (insteaded of Phosphor)
- Tic-TACS 0.1.2 (insteaded of C2ME/Barium)
